package juego;

import entorno.Entorno;

public class Disparo {
	
	private int x; //CREA LA COORDENADA EN X.
	private int y; //CREA LA COORDENADA EN Y.
	private int alto; //CREA EL ALTO DEL DISPARO.
	private int ancho; //CREA EL ANCHO DEL DISPARO.
	private SonidosImagenes dMedia;
	
	
	Disparo(int x, int y)
	{
		this.x = x;
		this.y = y;
		this.ancho = 20;
		this.alto = 10;
		this.dMedia = new SonidosImagenes();
		
	}
	
	void disparando(Entorno e)
	{
		this.x=this.x+3;
	}

	public void dibujarDisparo(Entorno e) {
		e.dibujarImagen(this.dMedia.getShoot(), this.x, this.y, 0);
		this.dMedia.getShootSound().start();
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	public int getAncho() {
		return ancho;
	}
	
	public int getAlto() {
		return alto;
	}
	
}
