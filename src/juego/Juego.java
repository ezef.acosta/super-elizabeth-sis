package juego;

import java.awt.Color;
import java.util.LinkedList;
import javax.swing.JOptionPane;
import java.util.Random;
import entorno.Entorno;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego
{
	
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;

	// Variables y m�todos propios de cada grupo
	private Princesa princess;
	private Obstaculo[] obstaculo;
	private LinkedList<Disparo> disparo;
	private SonidosImagenes media;
	private Soldado[] soldados;
	
	private boolean juegoIniciado;
	private boolean Terminado;
	private int puntosTotales, gano,crono,min,seg,vidas;

	Juego()
	{
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Super Elizabeth Sis - Grupo12 / Acosta - Lopez - Stegman / V1.00", 800, 600);
		this.juegoIniciado = false;
		this.princess = new Princesa(20);
		this.obstaculo = new Obstaculo[4];
		this.disparo=new LinkedList<>();
		this.media = new SonidosImagenes();// NUEVA VARIABLE CON OBJETOS DE IMG Y SOUND
		Random rSoldado = new Random();
		this.soldados = new Soldado[20 + rSoldado.nextInt(50)];
		
		this.seg=0;
		this.min=0;
		this.crono=0;
		this.vidas = 3;
		
		int PosX = 0;
		int ContAb = 0;
	
		for(int i = 0; i < this.obstaculo.length; i++) {
			
				if(ContAb < 2) {
					this.obstaculo[i] = new Obstaculo(PosX,485,50,50,Color.GREEN);
					ContAb = ContAb + 1;
					this.entorno.dibujarImagen(this.media.getObsChico(), PosX, ContAb, 0);
				}
				else {
					this.obstaculo[i] = new Obstaculo(PosX,476,50,70,Color.GREEN);
					ContAb = 0;
				}
			
			PosX += 500;
			this.Terminado = false;
			this.puntosTotales = 0;
			}
		
		// Inicia el juego!
		PosX = 300;
		for(int j = 0; j<soldados.length; j++){
			if(ContAb < 2){
				this.soldados[j] = new Soldado((PosX + rSoldado.nextInt(600)), 482);
				ContAb++;
			}
			else{
				this.soldados[j] = new Soldado((PosX + rSoldado.nextInt(600)),482);
				ContAb = 0;
			}
			PosX+=300;
		}		
		
		
		this.entorno.iniciar();
		
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	
	public void tick()
	{		
		if(juegoIniciado == false)
			{
				this.entorno.dibujarImagen(this.media.getBackMPpal(), 405, 310, 0);
				this.media.getInicio().loop(1);
				entorno.cambiarFont("Comic Sans MS", 35, Color.RED);
				entorno.escribirTexto("SELECCIONE UNA OPCION", 30, 100);
				entorno.escribirTexto("1 - INGRESAR (CTRL)", 10, 250);
				entorno.escribirTexto("2 - SALIR (ALT)", 10, 300);
				if (this.entorno.sePresiono(this.entorno.TECLA_CTRL))
				{
					this.juegoIniciado = true;
					this.media.getInicio().close();					
				}
				
				if(this.entorno.sePresiono(this.entorno.TECLA_ALT))
				{
					System.exit(0);
					this.media.getInicio().close();
				}
			}
	else {
			if (!Terminado) {
			this.entorno.dibujarImagen(this.media.getFondo(), 400, 300, 0);
			this.media.getNivel().loop(1);
			PuntajeyVidas(this.entorno);
			
////////////////////////////////////////////		
//DIBUJO DE SOLDADOS, VERIFICACION DE CONTACTO DEL DISPARO CON SOLDADO Y PRINCESA					
			
			for(int i=0; i<soldados.length; i++){
				if(this.soldados[i]!=null){
					this.soldados[i].dibujar(this.entorno);
					this.soldados[i].moverIzquierda();
					/*if(this.soldados[i].getX()<=1){
						this.soldados[i]= null;
						this.soldados[i] = this.soldados[i+1];
						//this.soldados.length = this.soldados.length+1;
					}*/
				}			
				if (soldados[i]!=null) {
					if(colisionSoldado(soldados[i])){
						this.vidas = this.vidas - 1;
						this.media.getHT();
						this.termino();
					}										
				}
			}
			
////////////////////////////////////////////
//DIBUJO DE LOS TUBOS EN PANTALLA		
			
			for(int i = 0; i < this.obstaculo.length; i++) {
				
				this.obstaculo[i].dibujar(this.entorno);
				this.obstaculo[i].moverIzq();
				
				if(this.obstaculo[i].getX()==0)
				{
					this.obstaculo[i].reposicionar(this.entorno.getWidth());
				}
				
////////////////////////////////////////////
//CONTACTO ENTRE EL PRINCESA Y EL TUBO O SUELO	
				
				if(colision(this.obstaculo[i]))
				{
					this.media.getHT().start();
					this.vidas = this.vidas - 1;
					this.termino();					
					
				}
				else if (this.princess.getX()==1)
				{
					this.media.getHF().start();
					this.termino();
				}
	
			}
///////////////////////////////////////////////	
//PROPIEDADES DE MOVIMIENTO DE PRINCESA, DISPARO Y COLICION DEL MISMO.	
			if(this.entorno.sePresiono(this.entorno.TECLA_ARRIBA))
			{
				if (this.princess.getY()==482)
					this.princess.moverArriba(this.entorno);
			}
			else
			{
				this.princess.moverAbajo(this.entorno);
				
			}
			
	 		if(this.entorno.sePresiono(this.entorno.TECLA_ESPACIO)) {
				this.disparo.add(this.princess.disparar());
			}
	 		if(this.entorno.estaPresionada(this.entorno.TECLA_DERECHA)&&this.princess.getX()!=400)
	 		{
	 			this.princess.caminar(this.entorno);
	 		}
	 		if(this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA)&&this.princess.getX()!=50)
	 		{
	 			this.princess.caminarAtras(this.entorno);
	 		}
	
			for(int i=0;i<this.soldados.length;i++) {
				if(this.soldados[i]!=null) {
					for(int j=0; j < this.disparo.size();j++) {
						if(hitDisparoSoldados(this.soldados[i],disparo.get(j))){							
							this.puntosTotales+=5;					
							this.soldados[i] = null;
							this.disparo.remove(j);
						}			
					}
				}
			}	 		
	 		
			cronoStart(this.entorno);
			
			for(int i=0;i<this.disparo.size();i++) {
				disparo.get(i).dibujarDisparo(this.entorno);
				disparo.get(i).disparando(this.entorno);
				if(disparo.get(i).getX() > 770) {
					this.disparo.remove(i);
				}
			}
			
				
//////////////////////////////////////////////////
//CONDICIONES DE FINALIZACION	
			
			if(this.princess.getY() == 550) {
				
				this.termino();
				}
			if(this.gano==0)
			{
				//this.ganoJuego(this.entorno);
				
			}
			
					//this.PuntajesyCron(this.entorno);
				}
			}
		}
	
	void PuntajesyCron(Entorno e) {
		
		e.cambiarFont("Serif", 50, Color.GRAY);
		e.escribirTexto("" + puntosTotales ,20.0, 40.0);	
		
	}
	void termino() {
		if(this.vidas==0){
			this.media.getNivel().close();
			this.media.getJGO().start();
			this.Terminado = true;
			JOptionPane.showMessageDialog(null, "Juego Terminado,su puntaje es: " + puntosTotales+ " y su tiempo fue de: " + this.min +":"+this.seg);
			System.exit(0);
		}
			
	}
	void ganoJuego(Entorno e)
	{
		this.media.getNivel().close();
		this.media.getJWin().start();
		this.Terminado = true;
		JOptionPane.showMessageDialog(null, "GANASTE!!, su puntaje es:" + puntosTotales+ " y su tiempo fue de: " + this.min+":"+this.seg);
		System.exit(0);

	}
	void PuntajeyVidas(Entorno e){
		this.entorno.cambiarFont("Serif", 20, Color.RED);
		this.entorno.escribirTexto("Puntos: " + this.puntosTotales +" /---/ " + "Vidas restantes: " + this.vidas, 10, 50);
	}
	
//////////////////////////////////////////////
//METODOS DE CONTACTOS

	boolean colision(Obstaculo p)
	{ 
		return  this.princess.getY() + this.princess.getDiametro()/2 <= p.getY() + p.getAlto()/2 &&
				this.princess.getY() + this.princess.getDiametro()/2 >= p.getY() - p.getAlto() /2 &&
				this.princess.getX() + this.princess.getDiametro()/2 == p.getX() - p.getAncho()/2 ||

				this.princess.getX() + this.princess.getDiametro()/2 <= p.getX() + p.getAncho()/2 &&
				this.princess.getX() + this.princess.getDiametro()/2 >= p.getX() - p.getAncho()/2 &&
				this.princess.getY() + this.princess.getDiametro()/2 == p.getY() - p.getAlto()/2 ||

				this.princess.getX() + this.princess.getDiametro()/2 <= p.getX() + p.getAncho()/2 &&
				this.princess.getX() + this.princess.getDiametro()/2 >= p.getX() - p.getAncho()/2 &&
				this.princess.getY() + this.princess.getDiametro()/2 == p.getY() + p.getAlto()/2 ; 		
	}

	boolean colisionSoldado(Soldado sold){
	 return this.princess.getY() + this.princess.getDiametro()/2 <= sold.getY() + sold.getAlto()/2 &&
			this.princess.getY() + this.princess.getDiametro()/2 >= sold.getY() - sold.getAlto() /2 &&
			this.princess.getX() + this.princess.getDiametro()/2 == sold.getX() - sold.getAncho()/2 ||

			this.princess.getX() + this.princess.getDiametro()/2 <= sold.getX() + sold.getAncho()/2 &&
			this.princess.getX() + this.princess.getDiametro()/2 >= sold.getX() - sold.getAncho()/2 &&
			this.princess.getY() + this.princess.getDiametro()/2 == sold.getY() - sold.getAlto()/2 ||

			this.princess.getX() + this.princess.getDiametro()/2 <= sold.getX() + sold.getAncho()/2 &&
			this.princess.getX() + this.princess.getDiametro()/2 >= sold.getX() - sold.getAncho()/2 &&
			this.princess.getY() + this.princess.getDiametro()/2 == sold.getY() + sold.getAlto()/2 ;
}

	boolean hitDisparoSoldados(Soldado sold, Disparo disparo){
		boolean hit = false;
		if(sold.getX() - sold.getAncho()/2 <= disparo.getX()&&
			sold.getX() + sold.getAncho()/2 >= disparo.getX()&&
			sold.getY() - sold.getAlto()/2 <= disparo.getY()&&
			sold.getY()+ sold.getAlto()/2 >= disparo.getY()){
				hit = true;
			}
			return hit;

	}

	void cronoStart(Entorno e)
	{
		this.crono +=10;
		if(this.crono == 1000)
		{
			this.seg++;
			this.crono = 0;			
		}
		if(this.seg == 60)
		{
			this.min++;	
			this.seg=0;		
		}
		
		e.cambiarFont("Serif", 30, Color.RED);
		e.escribirTexto(" " + this.min + " : " + this.seg, 580.0, 40.0);
	}

///////////////////////////////////////////////////////
	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		
		Juego juego = new Juego();
	}
}
