package juego;

import java.awt.*;

import entorno.Entorno;

public class Obstaculo {
	private int x,y,ancho,alto, desplazamiento; // CREA LAS VARIABLES DE POSICION Y TAMA�O DE LOS TUBOS
	private Color color; // CREA LA VARIABLE PARA EL COLOR
	private SonidosImagenes oMedia;

	
	public Obstaculo(int x, int y, int ancho, int alto, Color col) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.color = col;
		this.desplazamiento=1;
		this.oMedia = new SonidosImagenes();
	}
	
	void moverIzq(){
		this.x=this.x-this.desplazamiento;
	}
	

	void dibujar(Entorno e)
	{
		e.dibujarRectangulo(this.x, this.y, this.ancho,this.alto,0, this.color);
		if(this.alto==50) {
		e.dibujarImagen(this.oMedia.getObsChico(), this.x, this.y, 0);
		}
		else {
			e.dibujarImagen(this.oMedia.getObsGrande(), this.x, this.y, 0);
		}
	}

	int getX() {
		return this.x;
	}
	
	int reposicionar(int x)
	{
		return this.x = x;
	}
	
	int getY() {
		return this.y;
	}
	
	int getAncho() {
		return this.ancho;
	}
	
	int getAlto() {
		return this.alto;
	}
}
