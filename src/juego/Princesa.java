package juego;

import entorno.Entorno;

public class Princesa {
	private int diametro;// CREA VARIABLE DE DIAMETRO PARA MANEJAR CONTACTO
	private int x; // CREA VARIABLE DE POSICION X EN PANTALLA
	private int y; // CREA VARIABLE DE POSICION Y EN PANTALLA
	private int desplazamiento; // SE UTILIZA PARA SETEAR LA VELOCIDAD EN 0 CUANDO HAY CONTACTO.
	private SonidosImagenes pMedia;
	
	//CREA VARIABLES DE CARGA DE SONIDO


	public Princesa(int d){
		this.diametro = d;
		this.x=150;
		this.y=482;
		this.desplazamiento=3;
		this.pMedia = new SonidosImagenes();		
	}

	public void moverArriba(Entorno e)
	{	
		for (int n = 0;n<35;n++) {
			this.y -= 6.5;
			n++;
			DibujarJump(e);
		}
	}	
	 
	public void moverAbajo(Entorno e) {
		DibujarWalk(e);
		if(this.y<482)
		{
			this.y+=this.desplazamiento;
			DibujarWalk(e);
		}

	}
	public void caminar(Entorno e)
	{
		this.x +=2;
		DibujarWalk(e);
	}
	
	public void caminarAtras(Entorno e)
	{
		this.x -= 2;
		DibujarWalk(e);
	}
	
	
	//SECCION DE DIBUJADO IMG
	public void DibujarWalk(Entorno e){
		e.dibujarImagen(this.pMedia.getPrincesaW(), this.x, this.y, 0);
	}
	
	public void DibujarJump(Entorno e) {
		e.dibujarImagen(this.pMedia.getPrincesaJ(), this.x, this.y, 0);
		this.pMedia.getSJ();
	}
	
	public Disparo disparar(){
		Disparo disparo = new Disparo (this.x, this.y);
		return disparo;
	}
	//////////////////////////////

	public int getX() {
		return this.x;
	}
	
	public int getY() {
		return this.y;
	}
	
	public int getDiametro(){
		return this.diametro;
	}
	
}

