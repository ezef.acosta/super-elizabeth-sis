package juego;

	import java.awt.Color;

	import entorno.Entorno;
		
	public class Soldado {
	//variables de instancia
		
		private int x; 
		private int y;
		private int alto;
		private int ancho;
		private int velocidad;
		private SonidosImagenes sMedia;
	
		
		//constructor
		
		Soldado (int x, int y){
			this.x=x;
			this.y=y;
			this.ancho=11;
			this.alto=33;
			this.velocidad=-2;
			this.sMedia = new SonidosImagenes();
		}
			
		public void moverIzquierda() {
			this.x=this.x+velocidad;
		}
		
		public void dibujar (Entorno e) {
			//e.dibujarRectangulo(this.x, this.y, this.ancho, this.alto, 0, Color.blue);
			e.dibujarImagen(this.sMedia.getSoldado(), this.x, this.y, 0);
		}

		
		public int getAlto(){
			return this.alto;
		}

		public int getAncho(){
			return this.ancho;
		}

		public int getX(){
			return this.x;
		}

		public int getY(){
			return this.y;
		}
	}
