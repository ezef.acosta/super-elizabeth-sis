package juego;

import java.awt.Image;

import javax.sound.sampled.Clip;

import entorno.Herramientas;


public class SonidosImagenes {
	private Image fondo, backMPpal;	
	private Image princessWalk, princessJump;// CREA VARIABLES DE IMAGEN DE LA PRINCESA.
	private Image obs1, obs2; // CREA VARIABLES DE IMAGEN PARA LOS OBSTACULOS
	private Clip hitTube, hitFloor, feeding, soundJump, feedingBurg, change, inicio, nivel, jWin, jGO; //VARIABLES CLIP UTILIZADAS EN LA CLASE JUEGO
	private Clip shoot; // CREA LA VARIABLE DEL SONIDO DE DISPARO
	private Image shootIMG; // CREA LA VARIABLE DE LA IMAGEN DEL DISPARO
	private Image soldier; 
	
	public SonidosImagenes(){

		//IMAGENES
		this.fondo = Herramientas.cargarImagen("backGame.png");
		this.backMPpal = Herramientas.cargarImagen("BackgroundMPpal.gif");
		this.princessWalk = Herramientas.cargarImagen("PrincessWalk.gif");
		this.princessJump = Herramientas.cargarImagen("Princess.gif");
		this.obs1 = Herramientas.cargarImagen("tube1.png");
		this.obs2 = Herramientas.cargarImagen("tube2.png");
		this.shootIMG = Herramientas.cargarImagen("bola de fuego.png");
		this.soldier = Herramientas.cargarImagen("soldadoWalk.gif");
		
	
		//SONIDOS
		this.hitTube = Herramientas.cargarSonido("Hit_tube.wav");
		this.soundJump=Herramientas.cargarSonido("Jump.wav");
		this.inicio=Herramientas.cargarSonido("MenuPpal.wav");
		this.nivel=Herramientas.cargarSonido("Nivel.wav");
		this.jWin=Herramientas.cargarSonido("Gana.wav");
		this.jGO=Herramientas.cargarSonido("Pierde.wav");
		this.shoot=Herramientas.cargarSonido("Laser_shoot.wav");
		
		
		
	}
	
	//IMAGENES
	
	public Image getPrincesaW() {
		return this.princessWalk;
	}
	public Image getPrincesaJ() {
		return this.princessJump;
	}
	public Image getFondo() {
		return this.fondo;
	}
	
	public Image getBackMPpal() {
		return this.backMPpal;
	}
	
	public Image getObsChico() {
		return this.obs1;		
	}
	
	public Image getObsGrande() {
		
		return this.obs2;
	}
	
	public Image getShoot() {
		return this.shootIMG;
	}
	
	public Image getSoldado() {
		return this.soldier;
	}
	
	
	//SONIDOS
	
	public Clip getHT() {
		return this.hitTube;
	}


	public Clip getHF() {
		return this.hitFloor;
	}


	public Clip getSJ() {
		return this.soundJump;
	}

	public Clip getInicio() {
		return this.inicio;
	}


	public Clip getNivel() {
		return this.nivel;
	}


	public Clip getJWin() {
		return this.jWin;
	}


	public Clip getJGO() {
		return this.jGO;
	}

	public Clip getShootSound() {
		return this.shoot;
	}	
	
}
